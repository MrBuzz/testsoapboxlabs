from ctypes import *

def main():
    # Here is an example of how to integrate the .so (or .dylib) library into the python script.
    # The interface should have a method called 'count_erroneous_points',
    # taking the path of the .csv file as parameter and returning the amount of potentially erroneous points found
    # in the .csv file.

    interface_lib = './build/SoapBoxLib.dll' # or 'mylib.dylib' in case you are running it on a mac

    # Load shared library using ctypes (feel free to use a different method for loading the library)
    interface = CDLL(interface_lib)
    count_erroneous_points = interface.count_erroneous_points
    count_erroneous_points.argtypes = [c_char_p]
    count_erroneous_points.restype = c_int

    # Print the amount of erroneous points found in the data file.
    print("Amount of erroneous points:", count_erroneous_points("./datasets/inexistent.csv"))
    print("Amount of erroneous points:", count_erroneous_points("./datasets/data_points_valid.csv"))
    print("Amount of erroneous points:", count_erroneous_points("./datasets/data_points_empty_fields.csv"))
    print("Amount of erroneous points:", count_erroneous_points("./datasets/data_points_wrong_latitude.csv"))
    print("Amount of erroneous points:", count_erroneous_points("./datasets/data_points_wrong_longitude.csv"))

if __name__ == "__main__":
    main()
