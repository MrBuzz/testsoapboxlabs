cmake_minimum_required(VERSION 3.0.0)
project(SoapBoxLib VERSION 0.1.0)

include(CTest)
enable_testing()

add_library(SoapBoxLib SHARED src/SoapBoxLib.cpp src/CSVDataValidator.cpp src/Utils.cpp)
add_library(Catch INTERFACE)

set_property(TARGET SoapBoxLib PROPERTY CXX_STANDARD 17)
set_property(TARGET SoapBoxLib PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(SoapBoxLib PRIVATE include/ext)
target_include_directories(SoapBoxLib PRIVATE include)
target_include_directories(Catch INTERFACE include/ext)

add_executable(SoapBoxLib.Test src/SoapBoxLib.Test.cpp src/Utils.cpp)

target_link_libraries(SoapBoxLib.Test Catch)
target_link_libraries(SoapBoxLib.Test SoapBoxLib)

target_include_directories(SoapBoxLib.Test PRIVATE include/ext)
target_include_directories(SoapBoxLib.Test PRIVATE include)

set_property(TARGET SoapBoxLib.Test PROPERTY CXX_STANDARD 17)
set_property(TARGET SoapBoxLib.Test PROPERTY CXX_STANDARD_REQUIRED ON)

add_test(NAME SoapBoxLibTests COMMAND SoapBoxLib.Test)

