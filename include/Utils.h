#ifndef _UTILS_H_INCLUDED
#define _UTILS_H_INCLUDED
#pragma once

#include <SoapBoxLib.h>

namespace SoapBoxLib
{
class Utils
{
  public:
    static std::vector<std::string> split(const std::string &s, char delimiter);

    static bool greater(double lhs, double rhs, double epsilon = 0.001);
    static bool less(double lhs, double rhs, double epsilon = 0.001);
};

}; // namespace SoapBoxLib

#endif /* _UTILS_H_INCLUDED */