#ifndef _SOAPBOXLIB_H_INCLUDED 
#define _SOAPBOXLIB_H_INCLUDED 
#pragma once

#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <string_view>
#include <sstream>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <cmath>


// Do not mangle symbol names
#define CLINK extern "C"

#ifdef _WIN32
    #ifdef SoapBoxLib_EXPORTS
        #define SOAPBOXLIB_API  CLINK __declspec(dllexport)
    #else
        #define SOAPBOXLIB_API  CLINK __declspec(dllimport)
    #endif
#elif __APPLE__ || __linux__
    #define SOAPBOXLIB_API  CLINK
#else
    #error "Unknown compiler"
#endif


SOAPBOXLIB_API int count_erroneous_points(const char *path);

#endif