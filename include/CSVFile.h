#ifndef _CSVFILE_H_INCLUDED
#define _CSVFILE_H_INCLUDED
#pragma once

#include <SoapBoxLib.h>

namespace SoapBoxLib::CSV
{
/**
    This class represents a simple iterator for a CSV file, once loaded a file
    can be processed one line at a time. This object works with stl algorithms 
    such as std::for_each
*/
class CSVFile
{
  private:
    int m_record_size;
    std::ifstream m_infile;
    std::istream_iterator<std::string> m_infile_iterator;
    std::istream_iterator<std::string> m_end_of_iterator;

  public:
    explicit CSVFile(const char *path)
    {
        m_infile.open(path);
        m_infile_iterator = m_infile;
    }
   
    inline bool good() { return m_infile.is_open(); }
    inline std::istream_iterator<std::string> begin() { return m_infile_iterator; }
    inline std::istream_iterator<std::string> end() { return m_end_of_iterator; }
};
}; // namespace SoapBoxLib::CSV

#endif /* _CSVFILE_H_INCLUDED */