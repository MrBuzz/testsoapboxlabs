#ifndef _CSV_DATA_VALIDATOR_H_INCLUDED
#define _CSV_DATA_VALIDATOR_H_INCLUDED
#pragma once

#include <SoapBoxLib.h>
#include <Utils.h>

namespace SoapBoxLib::CSV
{
  /**
    This class implements the context independent validation policy for a line from a CSV file.
    If the number of columns in the CSV file is N the tokenization operation for a line must
    return N tokens. Otherwise there would be some missing data and the line 
    wouldn't be valid. There's a vistual method to implement context dependent validation policies
    that child classes can override to provide specific checks.
  */
    class CSVDataValidator
    {
      protected:
        int m_record_size = 0;

        bool checkRecordSize(std::vector<std::string> &tokens);
        virtual bool customChecks(const std::vector<std::string> &tokens) { return true; }
      
      public:
        explicit CSVDataValidator( int record_size) 
          :m_record_size(record_size)
          { }
        
        virtual ~CSVDataValidator() {}
        
        bool isValid(const std::string &s);
    };

  /**
    This class implements an example validation policy for geospatial data.
      => latitude must be between -90 and 90
      => longitude must be between -180 and 180
  */
    class CSVGeospatialDataValidator : public CSVDataValidator
    {
      public:
        explicit CSVGeospatialDataValidator( int record_size) 
          :CSVDataValidator(record_size)
          { }

        bool customChecks(const std::vector<std::string> &tokens);

      private:
        bool checkLatitude(const std::string &record);
        bool checkLongitude(const std::string &record);
    };
}; // namespace SoapBoxLib::CSV

#endif  /* _CSV_DATA_VALIDATOR_H_INCLUDED */