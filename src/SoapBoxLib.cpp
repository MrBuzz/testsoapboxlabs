#include <Utils.h>
#include <CSVFile.h>
#include <SoapBoxLib.h>
#include <CSVDataValidator.h>

using namespace SoapBoxLib::CSV;
using DataValidator = std::unique_ptr<CSVDataValidator>;

static const int GEOSPATIAL_RECORD_SIZE = 3;

SOAPBOXLIB_API int count_erroneous_points(const char *path)
{
    // init to -1 because it's the value to return in case of errors
    int erroneous_points = -1;
    CSVFile F{path};

    if(F.good())
    {
        erroneous_points = 0;
        // create the required validator based on the data format
        DataValidator Validator = std::make_unique<CSVGeospatialDataValidator>(GEOSPATIAL_RECORD_SIZE);

        // for each line in the file check if it's valid or not, 
        // if not increment erroneous_points.
        std::for_each(std::begin(F), std::end(F), 
            [&](const std::string &s) { 
                erroneous_points += (Validator->isValid(s)? 0 : 1);
            }
        ); 
    }
    
    return erroneous_points;
}