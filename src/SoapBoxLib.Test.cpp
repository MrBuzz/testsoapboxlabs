#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.h>
#include <SoapBoxLib.h>

SCENARIO( "Test against different datasets", "[map]" ) {

    GIVEN( "Some datasets to test" ) {
        
        std::map<std::string, std::string> datasets
        {
            {"INEXISTENT", "../datasets/inexistent.csv"},
            {"VALID", "../datasets/data_points_valid.csv"},
            {"EMPTY_FILEDS", "../datasets/data_points_empty_fields.csv"},
            {"WRONG_LATITUDE", "../datasets/data_points_wrong_latitude.csv"},
            {"WRONG_LONGITUDE", "../datasets/data_points_wrong_longitude.csv"}
        };

        REQUIRE( datasets.size() == 5 );

        WHEN( "The dataset is valid" ) {

            THEN( "The call to count_erroneous_points should return 0" ) {
                REQUIRE(  count_erroneous_points(datasets["INEXISTENT"].c_str()) == -1  );
            }
        }
        WHEN( "The dataset file does not exist" ) {

            THEN( "The call to count_erroneous_points should return -1" ) {
                REQUIRE(  count_erroneous_points(datasets["VALID"].c_str()) == 0  );
            }
        }
         WHEN( "The dataset file contains empty fields" ) {

            THEN( "The call to count_erroneous_points should return 5" ) {
                REQUIRE(  count_erroneous_points(datasets["EMPTY_FILEDS"].c_str()) == 5  );
            }
        }
         WHEN( "The dataset file contains wrong latitude fields" ) {

            THEN( "The call to count_erroneous_points should return 4" ) {
                REQUIRE(  count_erroneous_points(datasets["WRONG_LATITUDE"].c_str()) == 4  );
            }
        }
        WHEN( "The dataset file contains wrong longitude fields" ) {

            THEN( "The call to count_erroneous_points should return 4" ) {
                REQUIRE(  count_erroneous_points(datasets["WRONG_LONGITUDE"].c_str()) == 4  );
            }
        }
    }
}