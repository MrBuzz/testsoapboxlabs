#include <Utils.h>

std::vector<std::string> SoapBoxLib::Utils::split(const std::string &s, char delimiter)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

bool SoapBoxLib::Utils::greater(double lhs, double rhs, double epsilon)
{
    return (lhs - rhs) > ((std::abs(lhs) < std::abs(rhs) ? std::abs(rhs) : std::abs(lhs)) * epsilon);
}

 bool SoapBoxLib::Utils::less(double lhs, double rhs, double epsilon)
{
    return (rhs - lhs) > ((std::abs(lhs) < std::abs(rhs) ? std::abs(rhs) : std::abs(lhs)) * epsilon);
}
