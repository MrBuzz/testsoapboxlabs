#include <CSVDataValidator.h>

using namespace SoapBoxLib::CSV;

bool CSVDataValidator::isValid(const std::string &s)
{
    auto valid = true;

    std::vector<std::string> tokens = SoapBoxLib::Utils::split(s, ',');
    
    // check generic policy for the list of tokens
    valid &= checkRecordSize(tokens);

    // proceed only if the generic policy is satisfied  
    if(valid)   
        valid &= customChecks(tokens);

    return valid;
}


bool CSVDataValidator::checkRecordSize(std::vector<std::string> &tokens)
{
    auto result = true;

    // even if the size of tokens is correct, there may be some empty token
    if(tokens.size() == m_record_size)
    {
        if (std::any_of(std::begin(tokens), std::end(tokens), 
            [&](const std::string &s) { 
                return s.empty();
            }))
            result = false;
    }
    else
    {
        result = false;
    }
    
    return result;
}

bool CSVGeospatialDataValidator::customChecks(const std::vector<std::string> &tokens)
{
    auto valid = true;

    valid &= checkLatitude(tokens[0]);
    valid &= checkLongitude(tokens[1]);

    return valid;
}


bool CSVGeospatialDataValidator::checkLatitude(const std::string &record)
{
    auto result = false;
    auto latitude = atof(record.c_str());
    
    if (SoapBoxLib::Utils::greater(latitude, -90.00) &&
        SoapBoxLib::Utils::less(latitude, 90.00))
        result = true;

    return result;
}


bool CSVGeospatialDataValidator::checkLongitude(const std::string &record)
{
    auto result = false;
    auto longitude = atof(record.c_str());

    if (SoapBoxLib::Utils::greater(longitude, -180.00) &&
        SoapBoxLib::Utils::less(longitude, 180.00))
        result = true;

    return result;
}
